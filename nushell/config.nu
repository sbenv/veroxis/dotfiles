source ~/.config/nushell/utils.nu

$env.config = {
    show_banner: false # disable the welcome banner at startup
}

source ($nu.default-config-dir | path join "plugins" "starship.nu")
source ($nu.default-config-dir | path join "plugins" "zoxide.nu")

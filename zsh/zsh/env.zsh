BINSTALL_DISABLE_TELEMETRY=1

if [ "$TERM" = "xterm-ghostty" ]; then
    if ! infocmp -x >/dev/null 2>&1 ; then
        export TERM=xterm-256color
    fi
fi

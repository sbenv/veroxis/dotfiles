-- Set <space> as the leader key
-- See `:help mapleader`
--  NOTE: Must happen before plugins are loaded (otherwise wrong leader will be used)
vim.g.mapleader = " "
vim.g.maplocalleader = " "

vim.g.whitespace_tab_character = "» "
vim.g.whitespace_space_character = "⋅"

vim.g.static_workdir = vim.fn.getcwd()

vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.undodir = vim.fn.stdpath("data") .. "/undodir"
vim.opt.undofile = true

vim.opt.exrc = true
vim.opt.hidden = true
vim.opt.errorbells = false
vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true
vim.opt.smartindent = true
vim.opt.wrap = true
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.mouse = "a"
vim.opt.mousemoveevent = true
vim.opt.scrolloff = 8
vim.opt.colorcolumn = "120"
vim.opt.signcolumn = "yes"
vim.opt.list = true
---@diagnostic disable-next-line: missing-fields
vim.opt.listchars = {
    tab = vim.g.whitespace_tab_character,
    trail = vim.g.whitespace_space_character,
    multispace = vim.g.whitespace_space_character,
    lead = vim.g.whitespace_space_character,
}
vim.opt.termguicolors = true
vim.opt.incsearch = true
vim.opt.hlsearch = true
vim.opt.inccommand = "split"
vim.opt.ignorecase = true
vim.opt.smartcase = true
vim.opt.fixeol = true
vim.opt.clipboard = "unnamedplus"
vim.opt.showmode = false
vim.opt.updatetime = 250
vim.opt.timeoutlen = 300
vim.opt.splitright = true
vim.opt.splitbelow = true
vim.opt.cursorline = true
vim.opt.foldenable = false
vim.opt.whichwrap:append("<>,hl")
vim.opt.title = true
vim.opt.titlestring = "nvim {%{fnamemodify(getcwd(), ':~:t')}}"

vim.keymap.set("n", "Q", "<nop>", { desc = "disable quit with `Q`" })
vim.keymap.set("n", "<ESC>", function()
    vim.fn.setreg("/", "")
end, { desc = "clear search register", silent = true, noremap = false })

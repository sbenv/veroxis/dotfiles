vim.keymap.set("n", "<leader>om", function()
    vim.cmd.Mason()
end, { desc = "[O]pen [M]ason" })

vim.keymap.set("n", "<leader>ol", function()
    vim.cmd.Lazy()
end, { desc = "[O]pen [L]azy" })

vim.keymap.set("n", "<leader>ts", function()
    vim.cmd.Telescope()
end, { desc = "Open [T]ele[s]cope" })

vim.keymap.set(
    "t",
    "<ESC>",
    "<C-\\><C-n>",
    { desc = "Switch to normal mode by using the <ESC> key while in a terminal buffer" }
)

vim.keymap.set("n", "<C-left>", "<C-w><C-h>", { desc = "Move focus to the left window" })
vim.keymap.set("n", "<C-right>", "<C-w><C-l>", { desc = "Move focus to the right window" })
vim.keymap.set("n", "<C-down>", "<C-w><C-j>", { desc = "Move focus to the lower window" })
vim.keymap.set("n", "<C-up>", "<C-w><C-k>", { desc = "Move focus to the upper window" })

vim.keymap.set("v", "<leader>sa", ":'<,'>sort<CR>", { desc = "[S]ort selection in [a]scending order" })
vim.keymap.set("v", "<leader>sd", ":'<,'>sort!<CR>", { desc = "[S]ort selection in [d]escending order" })
vim.keymap.set("v", "<leader>sna", ":'<,'>sort n<CR>", { desc = "[S]ort selection [n]umerically in [a]scending order" })
vim.keymap.set(
    "v",
    "<leader>snd",
    ":'<,'>sort! n<CR>",
    { desc = "[S]ort selection [n]umerically in [d]escending order" }
)

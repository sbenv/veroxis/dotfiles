return {
    "romgrk/barbar.nvim",
    dependencies = {
        "lewis6991/gitsigns.nvim",
        "nvim-tree/nvim-web-devicons",
    },
    init = function()
        vim.g.barbar_auto_setup = false
    end,
    opts = {
        sidebar_filetypes = {
            NvimTree = true,
            undotree = {
                text = "undotree",
                align = "center",
            },
        },
        icons = {
            preset = "slanted",
            separator_at_end = false,
        },
    },
    version = "^1.0.0",
}

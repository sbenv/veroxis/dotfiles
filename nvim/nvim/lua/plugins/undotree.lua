return {
    "mbbill/undotree",
    event = "VimEnter",
    config = function(_, _)
        vim.keymap.set("n", "<leader>u", vim.cmd.UndotreeToggle, { desc = "[undotree] Toggle [u]ndotree" })
    end,
}

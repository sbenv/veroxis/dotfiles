return {
    "kyazdani42/nvim-tree.lua",
    version = "*",
    lazy = false,
    dependencies = {
        "kyazdani42/nvim-web-devicons",
    },
    config = function(_, _)
        assert(vim.opt.termguicolors:get() == true, "nvim-tree requires `vim.opt.termguicolors:get() == true`")
        vim.g.loaded_netrw = 1
        vim.g.loaded_netrwPlugin = 1

        require("nvim-tree").setup({
            filters = {
                enable = false,
            },
            renderer = {
                root_folder_label = false,
                highlight_clipboard = "name",
                group_empty = true,
                full_name = true,
                icons = {
                    web_devicons = {
                        file = {
                            enable = false, -- "plain theme"
                        },
                        folder = {
                            enable = false, -- "plain theme"
                        },
                    },
                    show = {
                        file = true,
                        folder = true,
                        folder_arrow = true,
                        git = true,
                        modified = true,
                        diagnostics = true,
                        hidden = false, -- highlight dotfiles
                        bookmarks = true,
                    },
                },
                indent_markers = {
                    enable = true,
                },
            },
            view = {
                adaptive_size = false,
                width = "200",
            },
            actions = {
                open_file = {
                    quit_on_open = false,
                    window_picker = {
                        enable = false,
                    },
                },
            },
            sync_root_with_cwd = false,
            respect_buf_cwd = true,
            update_focused_file = {
                enable = true,
                update_root = false,
            },
            modified = {
                enable = true,
            },
            diagnostics = {
                enable = true,
                show_on_dirs = true,
            },
            git = {
                enable = true,
                show_on_dirs = true,
                show_on_open_dirs = true,
            },
        })

        vim.keymap.set("n", "<C-f>", vim.cmd.NvimTreeToggle, { desc = "[NvimTree] Toggle the Project [F]iles browser" })
    end,
}

return {
    "NeogitOrg/neogit",
    event = "VimEnter",
    dependencies = {
        "nvim-lua/plenary.nvim", -- required
        "sindrets/diffview.nvim", -- optional
        "nvim-telescope/telescope.nvim", -- optional
    },
    config = true,
}

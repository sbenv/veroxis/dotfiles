return {
    "saecki/crates.nvim",
    ft = { "toml" },
    config = function(_, _)
        require("crates").setup({
            completion = {
                cmp = {
                    enabled = true,
                },
            },
        })
        require("cmp").setup.buffer({
            sources = { { name = "crates" } },
        })
    end,
}

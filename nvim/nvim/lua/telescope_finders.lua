local pickers = require("telescope.pickers")
local finders = require("telescope.finders")
local make_entry = require("telescope.make_entry")
local conf = require("telescope.config").values
local term_previewer = require("telescope.previewers.term_previewer")
local sorters = require("telescope.sorters")
local themes = require("telescope.themes")

local browse_projects = function(opts)
    if not vim.fn.executable("rg") then
        vim.notify("ripgrep is not installed", "warn")
        return nil
    end

    opts = opts or {}

    local home = vim.fn.getenv("HOME")

    local finder = finders.new_async_job({
        command_generator = function(_)
            return { "rg", "--files", "--hidden", "--color=never",
                "--glob", "!{.cache,.local,.config,.var}",
                "--glob",
                "**/.git/config",
                home,
            }
        end,
        entry_maker = function(path)
            local path_components = {}
            for component in path:gmatch("[^/]+") do
                table.insert(path_components, component)
            end

            -- remove last two entries to get convert "$project_path/.git/config" to "$project_path"
            table.remove(path_components)
            table.remove(path_components)

            path = "/" .. table.concat(path_components, "/")

            return {
                display = string.sub(path, #home + 2),
                ordinal = path,
                value = path,
            }
        end,
    })

    pickers.new(opts, {
        debounce = 100,
        prompt_title = "Discovered Git Projects",
        finder = finder,
        previewer = term_previewer.new_termopen_previewer(
            {
                get_command = function(entry)
                    -- convert to relative path starting in $HOME directory
                    local path = entry.value:sub(#home + 2)
                    if vim.fn.executable("eza") ~= 0 then
                        return {
                            "eza",
                            "-la",
                            "--icons", "always",
                            "--tree",
                            "--no-time",
                            "--no-permissions",
                            "--no-user",
                            "--no-filesize",
                            "--recurse",
                            "--level", "3",
                            "--absolute", "off",
                            "--git-ignore",
                            "--group-directories-first",
                            path,
                        }
                    end
                    -- using GNU ls as a fallback
                    return {
                        "ls",
                        "-l",
                        "--almost-all",
                        "--color=always",
                        path,
                    }
                end,
                cwd = home,
            }
        ),
        sorter = sorters.get_fzy_sorter(opts),
        attach_mappings = function(prompt_bufnr, map)
            map("i", "<CR>", function()
                local selection = require("telescope.actions.state").get_selected_entry()
                require("telescope.actions").close(prompt_bufnr)
                if selection ~= nil then
                    if vim.fn.isdirectory(selection.value) == 0 then
                        vim.notify("path is not a directory: " .. selection.value)
                        return
                    end
                    vim.api.nvim_set_current_dir(selection.value)
                    vim.opt.title = vim.opt.title -- update title after switching if it is enabled
                end
            end)
            return true
        end,
    }):find()
end

vim.keymap.set("n", "<C-p>", function()
    -- local opts = {}
    -- local opts = require("telescope.themes").get_cursor({})
    -- local opts = require("telescope.themes").get_dropdown({})
    local opts = themes.get_ivy({})
    browse_projects(opts)
end, { desc = "project browser" })


local live_multigrep = function(opts)
    if not vim.fn.executable("rg") then
        vim.notify("ripgrep is not installed", "warn")
        return nil
    end

    opts = opts or {}
    opts.cwd = opts.cwd or vim.uv.cwd()

    local finder = finders.new_async_job({
        command_generator = function(prompt)
            if not prompt or prompt == "" then
                return nil
            end

            local pieces = vim.split(prompt, "  ")
            local args = { "rg", "--hidden" }

            if pieces[1] then
                vim.list_extend(args, { "-e", pieces[1] })
            end

            if pieces[2] then
                vim.list_extend(args, { "-g", pieces[2] })
            end

            args = vim.list_extend(args,
                { "--color=never", "--no-heading", "--with-filename", "--line-number", "--column", "--smart-case" })

            return args
        end,
        entry_maker = make_entry.gen_from_vimgrep(opts),
        cwd = opts.cwd,
    })

    pickers.new(opts, {
        debounce = 100,
        prompt_title = "Multi Grep",
        finder = finder,
        previewer = conf.grep_previewer(opts),
        -- sorter = require("telescope.sorters").empty(),
        sorter = sorters.get_generic_fuzzy_sorter(opts),
        -- sorter = require("telescope.sorters").get_fzy_sorter(opts),
    }):find()
end

vim.keymap.set("n", "<leader>fg", function()
    -- local opts = {}
    -- local opts = require("telescope.themes").get_cursor({})
    -- local opts = require("telescope.themes").get_dropdown({})
    local opts = themes.get_ivy({})
    live_multigrep(opts)
end, { desc = "multigrep" })

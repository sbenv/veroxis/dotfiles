#!/usr/bin/env python3
from lib import CliInterface, PackageStore
from pathlib import Path

home = Path.home()

package_store = PackageStore.instance()

alacritty = package_store.create_package().name("Alacritty").root_path(f"{package_store.path()}/alacritty").build()
alacritty.create_symlink().target(f"{home}/.config/alacritty/alacritty.toml").source(f"{alacritty.path()}/alacritty.toml").build()

atuin = package_store.create_package().name("Atuin").root_path(f"{package_store.path()}/atuin").build()
atuin.create_symlink().target(f"{home}/.config/atuin/config.toml").source(f"{atuin.path()}/config.toml").build()

bash = package_store.create_package().name("Bash").root_path(f"{package_store.path()}/bash").build()
bash.create_symlink().target(f"{home}/.bashrc").source(f"{bash.path()}/.bashrc").build()
bash.create_symlink().target(f"{home}/.profile").source(f"{bash.path()}/.profile").build()

cosmic = package_store.create_package().name("Cosmic-Epoch").root_path(f"{package_store.path()}/cosmic").build()
cosmic.create_symlink().target(f"{home}/.config/cosmic").source(f"{cosmic.path()}/cosmic").build()

zsh = package_store.create_package().name("Zsh").root_path(f"{package_store.path()}/zsh").build()
zsh.create_symlink().target(f"{home}/.zshrc").source(f"{zsh.path()}/.zshrc").build()
zsh.create_symlink().target(f"{home}/.zprofile").source(f"{zsh.path()}/.zprofile").build()
zsh.create_symlink().target(f"{home}/.config/zsh").source(f"{zsh.path()}/zsh").build()

fish = package_store.create_package().name("Fish").root_path(f"{package_store.path()}/fish").build()
fish.create_symlink().target(f"{home}/.config/fish/config.fish").source(f"{fish.path()}/config.fish").build()

kitty = package_store.create_package().name("Kitty").root_path(f"{package_store.path()}/kitty").build()
kitty.create_symlink().target(f"{home}/.config/kitty").source(f"{kitty.path()}/kitty").build()

nvim = package_store.create_package().name("NeoVim").root_path(f"{package_store.path()}/nvim").build()
nvim.create_symlink().target(f"{home}/.config/nvim").source(f"{nvim.path()}/nvim").build()

starship = package_store.create_package().name("Starship").root_path(f"{package_store.path()}/starship-prompt").build()
starship.create_symlink().target(f"{home}/.config/starship.toml").source(f"{starship.path()}/starship.toml").build()

tmux = package_store.create_package().name("tmux").root_path(f"{package_store.path()}/tmux").build()
tmux.create_symlink().target(f"{home}/.tmux.conf").source(f"{tmux.path()}/.tmux.conf").build()

git = package_store.create_package().name("Git").root_path(f"{package_store.path()}/gitconfig").build()
git.create_symlink().target(f"{home}/.gitconfig.local").source(f"{git.path()}/.gitconfig.local").build()
git.create_symlink().target(f"{home}/.gitignore_global").source(f"{git.path()}/.gitignore_global").build()
git.create_symlink().target(f"{home}/.gitattributes").source(f"{git.path()}/.gitattributes").build()

ghostty = package_store.create_package().name("Ghostty").root_path(f"{package_store.path()}/ghostty").build()
ghostty.create_symlink().target(f"{home}/.config/ghostty").source(f"{ghostty.path()}/ghostty").build()

rust = package_store.create_package().name("Rust").root_path(f"{package_store.path()}/rust").build()
rust.create_symlink().target(f"{home}/.cargo/config.toml").source(f"{rust.path()}/config.toml").build()
rust.create_symlink().target(f"{home}/.cargo/env").source(f"{rust.path()}/env").build()

sheldon = package_store.create_package().name("Sheldon").root_path(f"{package_store.path()}/sheldon").build()
sheldon.create_symlink().target(f"{home}/.config/sheldon").source(f"{sheldon.path()}/sheldon").build()

bin = package_store.create_package().name("Scripts").root_path(f"{package_store.path()}/bin").build()
bin.create_symlink().target(f"{home}/.local/bin/mem_usage").source(f"{bin.path()}/mem_usage").build()
bin.create_symlink().target(f"{home}/.local/bin/snix").source(f"{bin.path()}/snix").build()

nushell = package_store.create_package().name("Nushell").root_path(f"{package_store.path()}/nushell").build()
nushell.create_symlink().target(f"{home}/.config/nushell/config.nu").source(f"{nushell.path()}/config.nu").build()
nushell.create_symlink().target(f"{home}/.config/nushell/env.nu").source(f"{nushell.path()}/env.nu").build()
nushell.create_symlink().target(f"{home}/.config/nushell/utils.nu").source(f"{nushell.path()}/utils.nu").build()
nushell.create_symlink().target(f"{home}/.config/nushell/plugins").source(f"{nushell.path()}/plugins").build()

CliInterface(package_store).run()
